﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ProjetAsp.Models;
using ProjetAsp.Service;
using ProjetAsp.Tests.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetAspTest.Service
{
    [TestClass]
    public class StatsServiceTest
    {
        Mock<ApplicationDbContext> mockContext;

        public void SetUp()
        {
            mockContext = EntityFrameworkMockHelper.GetMockContext<ApplicationDbContext>();

            var user1 = new ApplicationUser() { Id = "a", UserName = "a", Email = "p@hotmail.com", Trades = new List<Trade>(), items = new List<Item>(), Historique = new List<HistoriqueUser>() };
            var user2 = new ApplicationUser() { Id = "b", UserName = "b", Email = "f@hotmail.com", Trades = new List<Trade>(), items = new List<Item>(), Historique = new List<HistoriqueUser>() };
            var user3 = new ApplicationUser() { Id = "c", UserName = "c", Email = "p@hotmail.com", Trades = new List<Trade>(), items = new List<Item>(), Historique = new List<HistoriqueUser>() };
            var user4 = new ApplicationUser() { Id = "d", UserName = "d", Email = "f@hotmail.com", Trades = new List<Trade>(), items = new List<Item>(), Historique = new List<HistoriqueUser>() };
            var user5 = new ApplicationUser() { Id = "e", UserName = "e", Email = "p@hotmail.com", Trades = new List<Trade>(), items = new List<Item>(), Historique = new List<HistoriqueUser>() };
            var user6 = new ApplicationUser() { Id = "f", UserName = "f", Email = "f@hotmail.com", Trades = new List<Trade>(), items = new List<Item>(), Historique = new List<HistoriqueUser>() };

            var users = new List<ApplicationUser>();

            

            var cats = new List<Category>
            {
                new Category {CategoryID = 0, Name = "a", Items = mockContext.Object.itemsSet.Where(a => a.CategoryID == 0).ToList()},
                new Category {CategoryID = 1, Name = "b", Items = mockContext.Object.itemsSet.Where(a => a.CategoryID == 1).ToList()},
                new Category {CategoryID = 2, Name = "c", Items = mockContext.Object.itemsSet.Where(a => a.CategoryID == 2).ToList()},
                new Category {CategoryID = 3, Name = "d", Items = mockContext.Object.itemsSet.Where(a => a.CategoryID == 3).ToList()},
                new Category {CategoryID = 4, Name = "e", Items = mockContext.Object.itemsSet.Where(a => a.CategoryID == 4).ToList()},
                new Category {CategoryID = 5, Name = "f", Items = mockContext.Object.itemsSet.Where(a => a.CategoryID == 5).ToList()}
            }.AsQueryable();

            var itemsA = new List<Item>
            {
                new Item {ItemID = 0, UserID = "a", Available = false, CategoryID = 0, ItemName = "item1", User = user1, Category = cats.Where(c=>c.CategoryID == 0).First()},
                new Item {ItemID = 1, UserID = "a", Available = false, CategoryID = 2, ItemName = "item2", User = user1, Category = cats.Where(c=>c.CategoryID == 2).First()},
                new Item {ItemID = 2, UserID = "a", Available = false, CategoryID = 5, ItemName = "item3", User = user1, Category = cats.Where(c=>c.CategoryID == 5).First()},
                new Item {ItemID = 3, UserID = "a", Available = false, CategoryID = 2, ItemName = "item4", User = user1, Category = cats.Where(c=>c.CategoryID == 2).First()},
                new Item {ItemID = 4, UserID = "a", Available = false, CategoryID = 1, ItemName = "item5", User = user1, Category = cats.Where(c=>c.CategoryID == 1).First()},

            }.AsQueryable();
            var itemsB = new List<Item>
            {

            }.AsQueryable();
            var itemsC = new List<Item>
            {
                new Item {ItemID = 5, UserID = "c", Available = false, CategoryID = 0, ItemName = "item6", User = user3, Category = cats.Where(c=>c.CategoryID == 0).First()},
                new Item {ItemID = 6, UserID = "c", Available = false, CategoryID = 2, ItemName = "item7", User = user3, Category = cats.Where(c=>c.CategoryID == 2).First()},

            }.AsQueryable();
            var itemsD = new List<Item>
            {
                new Item {ItemID = 7, UserID = "d", Available = false, CategoryID = 0, ItemName = "item8", User = user4, Category = cats.Where(c=>c.CategoryID == 0).First()},
                new Item {ItemID = 8, UserID = "d", Available = false, CategoryID = 2, ItemName = "item9", User = user4, Category = cats.Where(c=>c.CategoryID == 2).First()},
                new Item {ItemID = 9, UserID = "d", Available = false, CategoryID = 5, ItemName = "item10", User = user4, Category = cats.Where(c=>c.CategoryID == 5).First()},
                new Item {ItemID = 10, UserID = "d", Available = false, CategoryID = 2, ItemName = "item11", User = user4, Category = cats.Where(c=>c.CategoryID == 4).First()},
                new Item {ItemID = 11, UserID = "d", Available = false, CategoryID = 1, ItemName = "item12", User = user4, Category = cats.Where(c=>c.CategoryID == 1).First()},

            }.AsQueryable();

            var trades = new List<Trade>
            {
                new Trade {TradeID = 0, UserID = "b", ItemID = 0, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsA.First(i => i.ItemID == 0), User = user2 },
                new Trade {TradeID = 1, UserID = "b", ItemID = 3, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsA.First(i => i.ItemID == 3), User = user2 },
                new Trade {TradeID = 2, UserID = "c", ItemID = 11, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsD.First(i => i.ItemID == 11), User = user3},
                new Trade {TradeID = 3, UserID = "d", ItemID = 5, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsC.First(i => i.ItemID == 5), User = user4 },
                new Trade {TradeID = 4, UserID = "a", ItemID = 6, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsC.First(i => i.ItemID == 6), User = user1 },
                new Trade {TradeID = 5, UserID = "a", ItemID = 10, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsD.First(i => i.ItemID == 10), User = user1 },
                new Trade {TradeID = 6, UserID = "b", ItemID = 7, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsD.First(i => i.ItemID == 7), User = user2 },
                new Trade {TradeID = 7, UserID = "f", ItemID = 2, StartDate = DateTime.Now, EndDate = new DateTime(2018, 9, 2), Item = itemsA.First(i => i.ItemID == 2), User = user6 }
            }.AsQueryable();



            user1.items = itemsA.ToList();
            user2.items = itemsB.ToList();
            user3.items = itemsC.ToList();
            user4.items = itemsD.ToList();

            user1.Trades = trades.Where(a => a.UserID == "a").ToList();
            user2.Trades = trades.Where(a => a.UserID == "b").ToList();
            user3.Trades = trades.Where(a => a.UserID == "c").ToList();
            user4.Trades = trades.Where(a => a.UserID == "d").ToList();
            user6.Trades = trades.Where(a => a.UserID == "f").ToList();
            users.Add(user1);
            users.Add(user2);
            users.Add(user3);
            users.Add(user4);
            users.Add(user5);
            users.Add(user6);

            var mockSetUsers = DbSetMocking.CreateMockSet<ApplicationUser>(users);

            mockContext.Setup(c => c.Users).Returns(mockSetUsers.Object);
            mockContext.Object.CategoriesSet.AddRange(cats);
            mockContext.Object.itemsSet.AddRange(itemsA);
            mockContext.Object.itemsSet.AddRange(itemsB);
            mockContext.Object.itemsSet.AddRange(itemsC);
            mockContext.Object.itemsSet.AddRange(itemsD);
            mockContext.Object.TradesSet.AddRange(trades);

        }
        [TestMethod]
        public void getTop5CategoryParSemaine()
        {
            SetUp();
            ServiceStatistic statsServ = new ServiceStatistic(mockContext.Object);

            var c = statsServ.getTop5CategoryParSemaine();
            int[] table = { 0, 2, 1, 4, 5 };
            int i = 0;
            foreach (Category item in c)
            {
                Assert.AreEqual(item.CategoryID, table[i]);
                i++;
            }
        }
        [TestMethod]
        public void getTop5CatsParMois()
        {
            SetUp();
            ServiceStatistic statsServ = new ServiceStatistic(mockContext.Object);
            
            var c = statsServ.getTop5CategoryParMois();
            int[] table = { 0, 2, 1, 4, 5 };
            int i = 0;
            foreach (Category item in c)
            {
                Assert.AreEqual(item.CategoryID, table[i]);
                i++;
            }
        }
        [TestMethod]
        public void getTop5UserObjetParSemaine()
        {
            SetUp();
            ServiceStatistic statsServ = new ServiceStatistic(mockContext.Object);

            var c = statsServ.getTop5UserObjetParSemaine();
            string[] table = { "b", "a", "c", "d", "f" };
            int i = 0;
            foreach (ApplicationUser item in c)
            {
                Assert.AreEqual(item.Id, table[i]);
                i++;
            }
        }

        [TestMethod]
        public void getTop5UserObjetParMois()
        {
            SetUp();
            ServiceStatistic statsServ = new ServiceStatistic(mockContext.Object);

            var c = statsServ.getTop5UserObjetParMois();
            string[] table = { "b", "a", "c", "d", "f" };
            int i = 0;
            foreach (ApplicationUser item in c)
            {
                Assert.AreEqual(item.Id, table[i]);
                i++;
            }
        }

        [TestMethod]
        public void getTop5AppreciationParSemaine()
        {
            SetUp();
            ServiceStatistic statsServ = new ServiceStatistic(mockContext.Object);

            var c = statsServ.getTop5UserApprecierParSemaine();
            
        }

    }
}
