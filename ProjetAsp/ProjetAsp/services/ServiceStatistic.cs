﻿using ProjetAsp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace ProjetAsp.Service
{
    public class ServiceStatistic
    {
        private ApplicationDbContext db;

        public ServiceStatistic(ApplicationDbContext y)
        {
            db = y;
        }
        /// <summary>
        /// retourne un IOrderedQueryable des top 5 de category par semaine (1ère liste par date =>2ième par key {Category,Trade})
        /// </summary>
        /// <returns></returns>
        public List<Category> getTop5CategoryParSemaine()
        {
            var u = DateTime.Now.AddDays(-7);

            List<Category> y = new List<Category>();
            foreach (Category cat in db.CategoriesSet)
            {
                y.AddRange(db.TradesSet.Where(o => o.EndDate >= u).Where(w => w.Item.CategoryID == cat.CategoryID).Select(o => o.Item.Category));
            }
            y = y.GroupBy(w => w.CategoryID).OrderByDescending(g => g.Count()).SelectMany(g => g).Distinct().Take(5).ToList();

            return y;
        }

        /// <summary>
        /// retourne un IOrderedQueryable des top 5 de category par mois (1ère liste par date =>2ième par key {Category,Trade})
        /// </summary>
        /// <returns></returns>
        public List<Category> getTop5CategoryParMois()
        {
            var u = DateTime.Now.AddDays(-30);

            List<Category> y = new List<Category>();
            foreach (Category cat in db.CategoriesSet)
            {
                y.AddRange(db.TradesSet.Where(o => o.EndDate >= u).Where(w => w.Item.CategoryID == cat.CategoryID).Select(o => o.Item.Category));
            }
            y = y.GroupBy(w => w.CategoryID).OrderByDescending(g => g.Count()).SelectMany(g => g).Distinct().Take(5).ToList();

            return y;

        }

        /// <summary>
        /// retourne un IOrderedQueryable des top 5 user selon le nombre d'objet par semaine (1ère liste par date =>2ième par key {int?,HistoriqueUser})
        /// </summary>
        /// <returns></returns>
        public List<ApplicationUser> getTop5UserObjetParSemaine()
        {
            var u = DateTime.Now.AddDays(-7);


            List<ApplicationUser> y = new List<ApplicationUser>();
            foreach (ApplicationUser cat in db.Users)
            {
                y.AddRange(db.TradesSet.Where(o => o.EndDate >= u).Where(w => w.User.Id == cat.Id).Select(o => o.User));
            }
            y = y.GroupBy(w => w.Id).OrderByDescending(g => g.Count()).SelectMany(g => g).Distinct().Take(5).ToList();

            return y;
        }

        /// <summary>
        /// retourne un IOrderedQueryable des top 5 user selon le nombre d'objet par mois (1ère liste par date =>2ième par key {int?,HistoriqueUser})
        /// </summary>
        /// <returns></returns>
        public List<ApplicationUser> getTop5UserObjetParMois()
        {
            var u = DateTime.Now.AddDays(-30);
            List<ApplicationUser> y = new List<ApplicationUser>();
            foreach (ApplicationUser cat in db.Users)
            {
                y.AddRange(db.TradesSet.Where(o => o.EndDate >= u).Where(w => w.User.Id == cat.Id).Select(o => o.User));
            }
            y = y.GroupBy(w => w.Id).OrderByDescending(g => g.Count()).SelectMany(g => g).Distinct().Take(5).ToList();

            return y;
        }

        /// <summary>
        /// retourne un IOrderedQueryable des top 5 user selon l'appreciation par semaine (1ère liste par date =>2ième par key {int?,HistoriqueUser})
        /// </summary>
        /// <returns></returns>
        public List<ApplicationUser> getTop5UserApprecierParSemaine()
        {
            var u = DateTime.Now.AddDays(-7);
            var h = db.HistoriqueSet.Where(o => o.date >= u)
                 .Select(y => y.User)
                 .OrderBy(i => i.Appreciation)
                 .Take(5)
                 .ToList();
            return h;
        }

        /// <summary>
        /// retourne un IOrderedQueryable des top 5 user selon l'appreciation par mois (1ère liste par date =>2ième par key {int?,HistoriqueUser})
        /// </summary>
        /// <returns></returns>
        public List<ApplicationUser> getTop5UserApprecierParMois()
        {
            var u = DateTime.Now.AddDays(-30);
            var h = db.HistoriqueSet.Where(o => o.date > u)
                 .Select(y => y.User)
                 .OrderBy(i => i.Appreciation)
                 .Take(5)
                 .ToList();
            return h;
        }


    }
}