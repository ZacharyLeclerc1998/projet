﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjetAsp.Models
{
    public class Item
    {
        [Key]
        [Display(Name = "ItemID", ResourceType = typeof(Ressources.Models.Item))]
        public int ItemID { get; set; }
        [Display(Name = "ItemName", ResourceType = typeof(Ressources.Models.Item))]
        public string ItemName { get; set; }
        [Display(Name = "CategoryID", ResourceType = typeof(Ressources.Models.Item))]
        public int CategoryID { get; set; }
        [InverseProperty("Items")]
        [Display(Name = "Category", ResourceType = typeof(Ressources.Models.Item))]
        public Category Category { get; set; }
        [Display(Name = "Available", ResourceType = typeof(Ressources.Models.Item))]
        public bool Available { get; set; }
        [InverseProperty("Item")]
        [Display(Name = "Trades", ResourceType = typeof(Ressources.Models.Item))]
        public virtual ICollection<Trade> Trades { get; set; }
        [ForeignKey("User")]
        [Display(Name = "UserID", ResourceType = typeof(Ressources.Models.Item))]
        public string UserID { get; set; }
        [InverseProperty("Items")]
        [Display(Name = "User", ResourceType = typeof(Ressources.Models.Item))]
        public virtual ApplicationUser User { get; set; }


        [Required]
        public int tempsMax { get; set; }
    }
    
}