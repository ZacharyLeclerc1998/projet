﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjetAsp.Models
{
    
    public class Category
    {
        [Display(Name = "CategoryID", ResourceType = typeof(Ressources.Models.Category))]
        public int CategoryID { get; set; }
        [Display(Name = "Name", ResourceType = typeof(Ressources.Models.Category))]
        public string Name { get; set; }
        [InverseProperty("Category")]
        [Display(Name = "Items", ResourceType = typeof(Ressources.Models.Category))]
        public virtual ICollection<Item> Items { get; set; }
    }
}