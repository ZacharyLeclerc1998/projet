﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjetAsp.Models
{
    public class HistoriqueUser
    {
        [Key]
        [Display(Name = "HistoriqueId", ResourceType = typeof(Ressources.Models.HistoriqueUser))]
        public int HistoriqueId { get; set; }
        [Display(Name = "UserID", ResourceType = typeof(Ressources.Models.HistoriqueUser))]
        public string UserID { get; set; }
        [InverseProperty("Historique")]
        [Display(Name = "User", ResourceType = typeof(Ressources.Models.HistoriqueUser))]
        public virtual ApplicationUser User { get; set; }
        [Display(Name = "date", ResourceType = typeof(Ressources.Models.HistoriqueUser))]
        public DateTime date { get; set; }

        
    }
}