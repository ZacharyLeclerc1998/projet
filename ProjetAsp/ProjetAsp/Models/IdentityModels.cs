﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjetAsp.Models
{
    // Vous pouvez ajouter des données de profil pour l'utilisateur en ajoutant plus de propriétés à votre classe ApplicationUser ; consultez http://go.microsoft.com/fwlink/?LinkID=317594 pour en savoir davantage.
    public class ApplicationUser : IdentityUser
    {
        [InverseProperty("User")]
        public virtual ICollection<Item> items { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<Trade> Trades { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<HistoriqueUser> Historique { get; set; }

        public virtual int Appreciation { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Notez qu'authenticationType doit correspondre à l'élément défini dans CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Ajouter les revendications personnalisées de l’utilisateur ici
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        protected override void OnModelCreating(DbModelBuilder db)
        {
            base.OnModelCreating(db);
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public virtual DbSet<Item> itemsSet{get;set;}
        public virtual DbSet<Trade> TradesSet { get; set; }
        public virtual DbSet<Category> CategoriesSet { get; set; }
        public virtual DbSet<HistoriqueUser> HistoriqueSet { get; set; }

       
    }
}