﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjetAsp.Models
{
    public class Trade
    {
        [Key]
        [Display(Name = "TradeID", ResourceType = typeof(Ressources.Models.Trade))]
        public int TradeID { get; set; }
        [Display(Name = "StartDate", ResourceType = typeof(Ressources.Models.Trade))]
        public DateTime StartDate { get; set; }
        [Display(Name = "EndDate", ResourceType = typeof(Ressources.Models.Trade))]
        public DateTime EndDate { get; set; }
        [ForeignKey("User")]
        [Display(Name = "UserID", ResourceType = typeof(Ressources.Models.Trade))]
        public string UserID { get; set; }
        [InverseProperty("Trades")]
        [Display(Name = "User", ResourceType = typeof(Ressources.Models.Trade))]
        public virtual ApplicationUser User { get; set; }
        [ForeignKey("Item")]
        [Display(Name = "ItemID", ResourceType = typeof(Ressources.Models.Trade))]
        public int ItemID { get; set; }
        [InverseProperty("Trades")]
        [Display(Name = "Item", ResourceType = typeof(Ressources.Models.Trade))]
        public virtual Item Item { get; set; }
    }
}