﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ProjetAsp.Models;

namespace ProjetAsp.Models
{
   
        public class DbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
        {
            protected override void Seed(ApplicationDbContext context)
            {

            #region User
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var role = new IdentityRole();
            role.Name = "admin";
            if (!roleManager.RoleExists("admin"))
                roleManager.Create(role);

            role = new IdentityRole();
            role.Name = "professeur";
            if (!roleManager.RoleExists("professeur"))
                roleManager.Create(role);
            role = new IdentityRole();
            role.Name = "etudiant";
            if (!roleManager.RoleExists("etudiant"))
                roleManager.Create(role);
            role = new IdentityRole();
            role.Name = "employe";
            if (!roleManager.RoleExists("employe"))
                roleManager.Create(role);

            var user = new ApplicationUser();
            user.UserName = "admin@admin.com";
            user.Email = "admin@admin.com";
            string userPass = "Passw0rd";
            var chkUser = userManager.Create(user, userPass);
            if (chkUser.Succeeded)
            {
                var result = userManager.AddToRole(user.Id, "admin");
            }

            var user2 = new ApplicationUser();
            user2.UserName = "prof@prof.com";
            user2.Email = "prof@prof.com";
            string userPass2 = "Passw0rd";
            var chkUser2 = userManager.Create(user2, userPass2);
            if (chkUser2.Succeeded)
            {
                var result = userManager.AddToRole(user2.Id, "professeur");
            }
            var user3 = new ApplicationUser();
            user3.UserName = "etudiant@etudiant.com";
            user3.Email = "etudiant@etudiant.com";
            string userPass3 = "Passw0rd";
            var chkUser3 = userManager.Create(user3, userPass3);
            if (chkUser3.Succeeded)
            {
                var result = userManager.AddToRole(user3.Id, "etudiant");
            }

            var user4 = new ApplicationUser();
            user4.UserName = "employe@employe.com";
            user4.Email = "employe@employe.com";
            string userPass4 = "Passw0rd";
            var chkUser4 = userManager.Create(user4, userPass4);
            if (chkUser4.Succeeded)
            {
                var result = userManager.AddToRole(user4.Id, "employe");
            }
            #endregion
            #region Categorie
            Category unecategorie = new Category();
            unecategorie.Name = "Tools";
            unecategorie.Items = new List<Item>();
            context.CategoriesSet.Add(unecategorie);
            Category deuxCategorie = new Category();
            deuxCategorie.Name = "Autre";
            deuxCategorie.Items = new List<Item>();
            context.CategoriesSet.Add(deuxCategorie);


            #endregion
            #region Item
            Item adminItem1 = new Item();
            adminItem1.Available = true;
            adminItem1.Category = unecategorie;
            adminItem1.ItemName = "Hammer";
            adminItem1.tempsMax = 2;
            adminItem1.User = user;
            context.itemsSet.Add(adminItem1);
            context.SaveChanges();
            #endregion
            base.Seed(context);
            }
        }
    
}