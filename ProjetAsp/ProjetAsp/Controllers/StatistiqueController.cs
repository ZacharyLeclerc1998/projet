﻿using ProjetAsp.Models;
using ProjetAsp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjetAsp.Controllers
{
    public class StatistiqueController : Controller
    {
        // GET: Statistique
        public ApplicationDbContext a = new ApplicationDbContext();
        public ServiceStatistic service = new ServiceStatistic(new ApplicationDbContext());
        public ActionResult IndexTop5UserAppreciationParMois()
        {
            ViewBag.liste = service.getTop5UserApprecierParMois();
            return View(service.getTop5UserApprecierParMois());
        }
        public ActionResult IndexTop5UserAppreciationParSemaine()
        {

            ViewBag.liste = service.getTop5UserApprecierParSemaine();
            return View(service.getTop5UserApprecierParSemaine());
        }
        public ActionResult IndexTop5UserObjetParMois()
        {

            ViewBag.liste = service.getTop5UserObjetParMois();
            return View(service.getTop5UserObjetParMois());
        }
        public ActionResult IndexTop5UserObjetParSemaine()
        {

            ViewBag.liste = service.getTop5UserObjetParSemaine();
            return View(service.getTop5UserObjetParSemaine());
        }
        public ActionResult IndexTop5CategoryParMois()
        {
            ViewBag.liste = service.getTop5CategoryParMois();
            return View(service.getTop5CategoryParMois());
        }
        public ActionResult IndexTop5CategoryParSemaine()
        {
            ViewBag.liste = service.getTop5CategoryParSemaine();
            return View(service.getTop5CategoryParSemaine());
        }
    }
}