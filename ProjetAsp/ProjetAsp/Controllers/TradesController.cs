﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjetAsp.Models;

namespace ProjetAsp.Controllers
{
    [Authorize(Roles = "admin, etudiant, professeur, employe")]
    public class TradesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Trades
        public ActionResult Index()
        {
            var tradesSet = db.TradesSet.Include(t => t.Item).Include(t => t.User);
            return View(tradesSet.ToList());
        }

        // GET: Trades/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trade trade = db.TradesSet.Find(id);
            if (trade == null)
            {
                return HttpNotFound();
            }
            return View(trade);
        }

        // GET: Trades/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trade trade = db.TradesSet.Find(id);
            if (trade == null)
            {
                return HttpNotFound();
            }
            ViewBag.ItemID = new SelectList(db.itemsSet, "ItemID", "ItemName", trade.ItemID);
            ViewBag.UserID = new SelectList(db.Users, "Id", "Email", trade.UserID);
            return View(trade);
        }

        // POST: Trades/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TradeID,StartDate,EndDate,UserID,ItemID")] Trade trade)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trade).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ItemID = new SelectList(db.itemsSet, "ItemID", "ItemName", trade.ItemID);
            ViewBag.UserID = new SelectList(db.Users, "Id", "Email", trade.UserID);
            return View(trade);
        }

        // GET: Trades/Delete/5
        public ActionResult Back(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trade trade = db.TradesSet.Find(id);
            if (trade == null)
            {
                return HttpNotFound();
            }
            return View(trade);
        }

        // POST: Trades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Trade trade = db.TradesSet.Find(id);
            db.TradesSet.Remove(trade);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
