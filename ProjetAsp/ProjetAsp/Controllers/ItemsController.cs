﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjetAsp.Models;
using Microsoft.AspNet.Identity;
using System.Globalization;
using System.Threading;

namespace ProjetAsp.Controllers
{
    
    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Items
        [Authorize]
        public ActionResult Index()
        {
            var itemsSet = db.itemsSet.Where(x=>x.Available == true).Include(i => i.Category).Include(i => i.User);
            
            return View(itemsSet.ToList());
        }

        public ActionResult CultureFR(string returnUrl)
        {
            Session["Culture"] = new CultureInfo("fr");
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(((CultureInfo)Session["Culture"]).Name);

            return RedirectToLocal(returnUrl);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult CultureEn(string returnUrl)
        {
            Session["Culture"] = new CultureInfo("en");
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["Culture"];
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(((CultureInfo)Session["Culture"]).Name);

            return RedirectToLocal(returnUrl);
        }

        [Authorize]
        // GET: Items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.itemsSet.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
        [Authorize]
        public ActionResult Emprunter(int? id)
        {

            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Item a = db.itemsSet.Find(id);
            if(a == null)
            {
                return HttpNotFound();
            }

            Trade unTrade = new Trade();
            a.Available = false;
            unTrade.Item = a;
            unTrade.StartDate = DateTime.Now;
            unTrade.UserID = User.Identity.GetUserId();

            unTrade.EndDate = unTrade.StartDate.AddDays(a.tempsMax);
            db.TradesSet.Add(unTrade);
            db.Entry(a).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        [Authorize]
        // GET: Items/Create
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.CategoriesSet, "CategoryID", "Name");
           
            return View();
        }

        // POST: Items/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "ItemID,ItemName,CategoryID,tempsMax")] Item item)
        {
            
            if (ModelState.IsValid)
            {
                item.UserID = User.Identity.GetUserId();
                item.Available = true;
                item.Trades = new List<Trade>();
                db.itemsSet.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.CategoriesSet, "CategoryID", "Name", item.CategoryID);
            //ViewBag.UserID = new SelectList(db.Users, "Id", "Email", item.UserID);
            return View(item);
        }

        // GET: Items/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.itemsSet.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.CategoriesSet, "CategoryID", "Name", item.CategoryID);
            //ViewBag.UserID = new SelectList(db.Users, "Id", "Email", item.UserID);
            return View(item);
        }

        // POST: Items/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult Edit([Bind(Include = "ItemID,ItemName,CategoryID,Available,UserID")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.CategoriesSet, "CategoryID", "Name", item.CategoryID);
            //ViewBag.UserID = new SelectList(db.Users, "Id", "Email", item.UserID);
            return View(item);
        }

        // GET: Items/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.itemsSet.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.itemsSet.Find(id);
            db.itemsSet.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
